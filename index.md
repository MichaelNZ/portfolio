---
layout: default
---

# Story and Timeline/Overview

Having completed Project 1, I was apprehensive of being the only continuing team member left in Dunedin. The rest of the team had been Project 2 students and Josh was completing the project from Auckland. 

During the holidays, I had created a Lecturer's Guide to GitLab, a basic introduction to GitLab for lecturers ([see here](https://gitlab.com/op-bit-platform/documentation/blob/master/Gitlab/Lecturers%20Guide%20to%20GitLab.docx)). I had a session with Lewis, who explained all the things that I needed to know to take over from him as the person on the ground. Thankfully, he was still on hand for the first few weeks to smooth the transition. 

One change that I had not expected was that the entire BIT department had agreed to switch to GitLab for code control, replacing GitHub and GitBucket (except for two papers). We were under the impression that Patricia would try out GitLab with her two programming papers this semester before bringing the entire department on board next year. However, this was not the case and we were faced with the prospect of GitLab being used far more than we had anticipated.

I had previously written documentation for GitLab, but it needed to be updated due to the fact that class repositories were created by scripts based on class lists. I edited the documentation to walk the students through setting up their class repositories. Also, during the break, Josh and Lewis had set up a ticketing system called Zammad. I put the email address in the documentation. You can see the documentation [here](https://gitlab.op-bit.nz/inglism1/PortfolioEvidence/raw/master/Gitlab+How+To+Guide.docx)

We actually got a ticket very early on: Patricia had attempted to sign into GitLab, which last semester had been integrated with the Polytechnic’s single sign-on, but had been redirected to a screen that said “State Information Lost”. It was determined that the problem was clicking on the link from the documentation, so I removed the link. This was a temporary fix, however, for the problem.
![The ticket](images/ticket.PNG)

<img align="right" width="200" src="images/cmach.jpg">
On the first day of semester 2, we had four students join the BIT Platform team: Lee, William, Aleen and Carlin. It fell to me to set up user accounts for them in our Active Directory system and assign them all client machines. This was a new feature that Lewis had set up – rather than having to set one’s DNS servers to access the staging environment, each member of the team was assigned a client machine which they could access through Remote Desktop Connection. After Lewis granted me administrator access to GitLab and Mattermost, I was able to add Lee as an administrator to both applications.

After we had received class lists from lecturer, Lewis showed me how to run the script to create the repositories. 

In our first team meeting, I was tasked with finding an alternative to Trello, a Kanban board application. As we already had our own self-hosted Git server and instant messaging application (to replace GitHub and Slack), Adon wanted a self-hosted Kanban board application as well. 

This led me into the world of Kanban alternatives and project management applications. We wanted something that was 1) free/open source; 2) self-hosted; and 3) compatible with SAML which would enable us to use the single sign-on. There were very few applications that met these criteria. Two that I investigated were Taiga and Lavagna. I set up virtual machines in the staging environment in order to test each one. Lavagna, it turned out, didn’t support SAML, but it did support signing in through GitLab.

However, the quest for a self-hosted Kanban alternative was put to an end by Adon’s discovery of GitLab Issues. This feature of GitLab enables users to create a Kanban Board for each GitLab project. Like Trello, you can create lists, create “cards” (issues), move issues from one list to another, assign users to issues, and you pretty much have a fully fledged Kanban Board in GitLab.

Early on in the semester, we all signed non-disclosure agreements. Since we had access to students’ personal information though GitLab, we promised that we would not access any information that was not necessary. I felt that this was a big responsibility and a foreshadowing of the role of a systems administrator in the real world.

We encountered a problem between GitLab and the single sign-on, where someone already logged in on the single sign-on who tried to access GitLab was sent to the same screen that Patricia had encountered on the first day, with the message 'State Information Lost'. Lee and I looked into simpleSAMLphp, the middle man between GitLab and the Polytechnic's single sign-on, and found that this was the problem. However, we weren't able to fix it at this stage, and had a few incidents in the semester where students were encountering the 'State Information Lost' screen.

An issue that came up relatively early on was that students did not have .gitignore files. A .gitignore file is necessary to stop certain files (such as compiled binaries) being uploaded to GitLab. These files, which are unnecessary, take up space on the server. The GitLab server only has 40GB of storage space available, so we needed to make sure that space was not taken up unnecessarily. Programming 2 students were contributing to this, so after talking to Joy about the issue, I created a guide on how to create a .gitignore file for both Visual Studio and Java. You can read the documentation [here](https://gitlab.com/op-bit-platform/documentation/blob/master/Gitlab/Creating%20a%20gitignore%20file.docx)

One issue that came up during the semester was whether to continue using Mattermost. Originally deployed as a self-hosted Slack alternative that was eventually intended to completely replace Slack, it was only being used by our project team due to the departure of Christopher, who has previously used it for his papers. It had been planned to turn Slack off this semester and use only Mattermost, but this didn’t happen and was scheduled for the beginning of next year. I argued that we should continue using Mattermost and should try and convince the staff to adopt it. However, the rest of the team disagreed and it was decided that we would switch off Mattermost.

My next task was looking into GitLab Pages. GitHub, in addition to being a code repository, allows users to host websites using Jekyll. Many students had used GitHub Pages to host their portfolio pages for their projects. Since GitLab is supposed to be a fully-fledged GitHub alternative, we wanted students to be able to host their portfolios on GitLab Pages. Josh had already managed to set this up on the staging environment, so it was up to me to implement this feature on the production environment. This involved configuring a shared runner and obtaining tokens. I first started by looking at the staging environment and set up a page there. Lee and I configured a shared runner on the GitLab production environment. We got Rob to set up a DNS entry to enable access to the pages. However, this wasn't enough to get the pages running. I contacted Josh and he said that I needed a ci and gemfile. After obtaining those, we eventually managed to get a page working, but we had to push the file from the desktop in order to update the pipeline. GitLab Pages were at the stage where a page would be displayed in a subdirectory.
![chat](images/joshchat.JPG)

After the break, Adon and I went to see a couple of guys from the Mornington Methodist Church regarding a side project. They wanted to get wireless internet set up in the church and have displays on the TV during worship. An individual church website was also discussed (at present, they share a website with the other Methodist churches in Dunedin) We discussed the issues with them, but Adon later said that what they were essentially looking for did not really constitute an IT project, so I had no further contact with the church.

We were approached by the Internet of Things project group to create a server for them on AWS. The staging environment that we use for testing is not accessible from outside the Polytechnic, so they wanted an outward-facing server. As I was the only one who had a login for AWS, it fell to me to create the server. The issue was complicated by the fact that the group had already configured a server on a virtual machine, and wanted it transferred to AWS rather than having to set it up again. This involved a significant amount of research, as Amazon had no straightforward documentation on how to do this. Lee managed to export the virtual machine in the required file format, and we began the import into AWS through the console – a long process. It worked, and after exposing a number of ports, the Internet of Things team were able to sign into their server.
![awsiotserver](images/awsiot.JPG)
![awsserverimport](images/iotimport.JPG)

<img align="left" width="200" src="images/icingagl.jpg">
We kept getting notifications on Icinga, our monitoring system, that GitLab’s memory was often hitting the warning level. We agreed to increase the memory level, but due to the way that AWS works, we had to upgrade the virtual machine to the next tier. This required the machine to be powered off. We put out a notification on Slack that GitLab would be down, and then ran the upgrade. Doing this upgrade also changed the DNS entry, so we had to update that as well. The upgrade took less than half an hour.
![instancedetails](images/glaws.JPG)


Throughout the semester, I have also dealt with students’ enquiries about GitLab and reset passwords. I feel that this is good practice for working as a systems administrator.

My final task was looking into vRealize Automation, the new platform for creating virtual machines that will replace vCloud next year. I had to do a bit of research, and found that the documentation for this platform is anything but clear. I discovered that there is a third level of user, the Tenant Administrator, which sits in between the System Administrator (the “god” level) and the regular user. Rob set me as a Tenant Administrator and I began exploring the system. Having used VCSA to set up virtual machines in the staging environment, I found vRealize Automation to be inferior. The user interface was not as user-friendly. The first few virtual machines I tried to create failed, all with the same error message. However, after demonstrating the process to Adon and Nathan, my first virtual machine succeeded.

Rob then gave me an additional user role on the system, which allowed me to create machine blueprints. These are used to create virtual machines. I managed to create templates for an Ubuntu machine and a Windows Server machine and bring up virtual machines using these blueprints. I documented the process in a guide for those who will use the platform next year. The documentation can be found [here](https://gitlab.com/op-bit-platform/documentation/blob/master/vRealise%20Automation.docx).

I feel that this project has continued to educate me in the role of a systems administrator. I definitely feel that the level of responsibility was increased from last semester, as the majority of the BIT department was using GitLab. I am glad to have been introduced to the world of virtualization, particularly through using AWS and creating a server for another project group. Bringing the new students up to speed was also good experience for working in the systems administration industry. I am glad that we have managed to fulfil the terms of our Service Level Agreement that I wrote last semester.

# Technical Proficiency

The variety of tasks that I undertook in this project have increased my technical proficiency in a number of areas, including virtualization and user management on Active Directory. I feel flattered to be the only person in the team with access to AWS, despite never having used it, and I am proud of my achievements with it.

I not sure to which extent I followed best practices, but I tried to make sure that my solutions were top-quality. I used GitLab as appropriate version control for the documentation. I feel that I contributed slightly less than an equal portion of the project. While I undertook side projects (the Internet of Things server, vRealize Automation) that were important, I did not feel that I has as significant a role in the development of the new gateway and database. I also was not involved in the shift of GitLab Pages to the new API, mainly due to sickness at the time Lee was doing this.

# Professional Proficiency

I feel that I maintained a high level of professional proficiency. I communicated well with the other team members and I almost always attended meetings (when they were held!) I recorded the meetings and kept the recordings in a repository which can be found [here](https://gitlab.com/op-bit-platform/meetings). I produced documentation throughout my time on the project, and tried to be as precise as possible in such documentation. I feel that I adapted well to changing requirements.

# CV

You can see my CV [here](CVMichaelInglis.pdf).